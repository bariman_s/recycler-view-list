package com.example.brizz.recyclerviewlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.LinkedList;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

public class MainActivity extends AppCompatActivity {

    LinkedList<String> listBuah;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listBuah = new LinkedList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        listBuah.add("1. Buah Apel");
        listBuah.add("2. Buah Mangga");
        listBuah.add("3. Buah Semangka");
        listBuah.add("4. Buah Jeruk");
        listBuah.add("5. Buah Pisang");
        listBuah.add("6. Buah Alpukat");
        listBuah.add("7. Buah Anggur");
        listBuah.add("8. Buah Semangka");
        listBuah.add("9. Buah Manggis");
        listBuah.add("10. Buah Nanas");
        listBuah.add("11. Buah Durian");
        listBuah.add("12. Buah Belimbing");
        listBuah.add("13. Buah Ceri");
        listBuah.add("14. Buah Duku");
        listBuah.add("15. Buah Jeruk");
        listBuah.add("16. Buah Kedongdong");
        listBuah.add("17. Buah Kelengkeng");
        listBuah.add("18. Buah Kiwi");
        listBuah.add("19. Buah Markisa");
        listBuah.add("20. Buah Naga");

        BuahAdapter adapter = new BuahAdapter(listBuah);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



    }
}
