package com.example.brizz.recyclerviewlist;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;

/**
 * Created by brizz on 10/15/17.
 */

public class BuahAdapter extends RecyclerView.Adapter<BuahAdapter.MyViewHolder> {

    private LinkedList<String> buah;

    public BuahAdapter(LinkedList<String> buah) {
        this.buah = buah;
    }

    @Override
    public BuahAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.buah_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BuahAdapter.MyViewHolder holder, int position) {

        holder.tvBuah.setText(buah.get(position));
    }

    @Override
    public int getItemCount() {
        return buah.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvBuah;
        public MyViewHolder(View itemView) {
            super(itemView);

            tvBuah = itemView.findViewById(R.id.tvBuah);

        }
    }
}
